import './App.css';
import MnkBoard from './MnkBoard';

function App() {
  let m, n, k;
  m = 15;
  n = 15;
  k = 5;
  return (
    <div className="App">
      <MnkBoard m={m} n={n} k={k} />
    </div>
  );
}

export default App;
