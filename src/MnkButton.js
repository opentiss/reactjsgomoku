import { Avatar } from '@mui/material';
import Box from '@mui/material/Box';
import React from 'react';
import './MnkButton.css';

class MnkButton extends React.Component {
    constructor(props) {
        super(props);
        this.state = {value: null, bgcolor: null};
    }

    render () {
        return (<Box sx={this.props.style} onClick={e => this.props.onClick(e, this, this.props.m, this.props.n)} onMouseOver={e => this.props.onMouseOver(e, this, this.props.m, this.props.n)} onMouseOut={e => this.props.onMouseOut(e, this, this.props.m, this.props.n)} >
            <Avatar sx={{height: this.props.style.height, width: this.props.style.width, fontSize: this.props.style.fontSize, bgcolor: this.state.bgcolor, display: this.state.bgcolor === null ? 'none': 'flex'}} children={this.state.value === null ? ' ' : this.state.value} ></Avatar>
        </Box>);
    }
}

export default MnkButton;