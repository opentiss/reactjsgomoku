import logo from './logo.svg';
import React from 'react';
import * as THREE from 'three';
import MnkButton from './MnkButton';
import './MnkBoard.css';
import InputLabel from '@mui/material/InputLabel';
import Box from '@mui/material/Box';
import FormControl from '@mui/material/FormControl';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import BorderAllIcon from '@mui/icons-material/BorderAll';
import RestartAltIcon from '@mui/icons-material/RestartAlt';
import { Undo } from '@mui/icons-material';
import { Avatar, IconButton, LinearProgress } from '@mui/material';
import { BorderRight } from '@mui/icons-material';

class MnkBoard extends React.Component {
    constructor(props) {
        super(props);
        this.initBoard(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.handleMouseover = this.handleMouseover.bind(this);
        this.handleMouseout = this.handleMouseout.bind(this);
        this.updateBoard3D = this.updateBoard3D.bind(this);

    }

    initBoard(scope) {
        let state = {
            history: [],
            player: 0,
            step: 0,
            timer: 90,
            countdown: 90,
            win: null,
        };
        if (!scope.state) {
            state.up = Date.now();
            state.height = window.innerHeight;
            state.buttons = Array(this.props.m).fill(Array(this.props.n).fill(null));
            state.m = this.props.m;
            state.n = this.props.n;
            state.k = this.props.k;
            state.cb = '#D2C0B2';
            state.cw = '#FA11DA';
            state.width = window.innerWidth;
            scope.state = state;
            setInterval(()=> {
                if (this?.state?.history?.length) {
                    if (this.state.countdown > 0) {
                        this.setState({countdown: this.state.countdown - 1});
                    }
                    else {
                        this.setState({win: this.state.player === 0 ? 1 : 0, player: null});
                    }
                }
            }, 1000);

        }
        else {
            state.up = scope.state.up;
            if (scope.state.history.length) {
                scope.state.history.forEach(e => e.setState ({className: null, value: null, bgcolor: null}));
            }
            state.buttons = Array(scope.state.m).fill(Array(scope.state.n).fill(null));
            scope.setState(state);
        }

    }

    transpose(buttons) {
        let buttond = [];
        buttons.forEach ((nv, ni) => {
            nv.forEach((mv, mi) => {
                if (!buttond[mi]) {
                    buttond[mi] = [];
                }
                buttond[mi].push(mv);
            })

        });
        return buttond;
    }

    calculateMnk (buttons, ak, k) {
        let ai = 0;
        return buttons.findIndex((nv, ni) => {
            return (nv.findIndex ((mv, mi) => {
                if (mv === ak[ai]) {
                    if (++ai === k) {
                        return true;
                    }
                }
                else {
                    ai = 0;
                }
                return false;

            }) !== -1)
        });

    }

    calculateMnkWinner (buttons, player, m, n, k) {
        let win = null, ak = Array(k).fill(player);
        let v = this.calculateMnk (buttons, ak, k);
        if (v === -1) {
            v = this.calculateMnk (this.transpose(buttons), ak, k);
    
        }
        if (v === -1) {
            let buttond = [];
            buttons.forEach ((nv, ni) => {
                nv.forEach((mv, mi) => {
                    if (!buttond[ni + mi]) {
                        buttond[ni + mi] = [];
                    }
                    buttond[ni + mi].push(mv);
                })

            });
            v = this.calculateMnk (buttond, ak, k);
    
        }
        if (v === -1) {
            let buttond = [];
            buttons.forEach ((nv, ni) => {
                nv.forEach((mv, mi) => {
                    if (ni < mi) {
                        if (!buttond[ni - mi + n]) {
                            buttond[ni - mi + n] = [];
                        }
                        buttond[ni - mi + n].push(mv);
                    }
                    else if (ni > mi) {
                        if (!buttond[ni - mi]) {
                            buttond[ni - mi] = [];
                        }
                        buttond[ni - mi].push(mv);
                    }
                    else {
                        if (!buttond[ni - mi]) {
                            buttond[ni - mi] = [];
                        }
                        buttond[ni - mi].push(mv);
    
                    }
                })

            });
            v = this.calculateMnk (buttond, ak, k);
    
        }

        if (v !== -1) {
            win = player;
        }
        return win;

    }

    handleChange (e) {
        if (e.target.value.m && e.target.value.n) {
            this.setState ({m: e.target.value.m, n: e.target.value.n, buttons: Array(e.target.value.m).fill(Array(e.target.value.n).fill(null))}, this.updateBoard3D);
        }
        else if (typeof Number.parseInt(e.target.value) === 'number') {
            this.setState ({k: Number.parseInt(e.target.value)});
        }
    }

    handleClick (e, r, m, n) {
        if (e?.target?.setState) {
            this.setState({step: this.state.step + 1});
            e.target.setState ({value: this.state.step});
        }
        else if (r?.setState && (this?.state?.win === null)) {
            let needUpdate;
            const copied = this.state.buttons.map((nv, ni) => {
                return nv.map((mv, mi) => {
                    if ((n === ni) && (m === mi) && (mv === null)) {
                        let step = this.state.step + 1;
                        let player = this.state.player;
                        this.setState({step: step});
                        let bgcolor;
                        
                        if (this.state.player === 0) {
                            bgcolor = this.state.cb;
                        }
                        else if (this.state.player === 1) {
                            bgcolor = this.state.cw;
                        }

                        r.setState ({bgcolor: bgcolor, player: player, value: step, className: 'MnkPlayer' + player});

                        needUpdate = true;
                        return player;
                    }
                    else {
                        return mv;

                    }
                });
            });
            if (needUpdate) {
                let player = this.state.player;
                let win = this.calculateMnkWinner(copied, player, this.state.m, this.state.n, this.state.k);
                if (player === 0) {
                    player = 1;
                }
                else if (player === 1) {
                    player = 0;
                }
                let history = [];
                if (this.state.history) {
                    history = this.state.history.map(e => e);
                }
                history.push(r);
                let state = {buttons: copied, history: history};
                if (win != null) {
                    state.player = null;
                    state.win = win;
                    //this.setState({player: null, win: win});
                }
                else {
                    state.player = player;
                    state.countdown = this.state.timer;
                    //let last = Date.now();
                    //this.setState({buttons: copied, player: player}, () => {console.log(Date.now() - last)});
                }
                //scope.state.countdown
                this.setState(state);
            }

        }

    }

    handleMouseover (e, r, m, n) {
        if (r?.setState && this?.state?.buttons && (this?.state?.win === null)) {
            this.state.buttons.forEach((nv, ni) => {
                return nv.forEach((mv, mi) => {
                    if ((n === ni) && (m === mi) && (mv === null)) {
                        let player = 'MnkPlayer' + this.state.player;
                        let bgcolor;
                        
                        if (this.state.player === 0) {
                            bgcolor = this.state.cb;
                        }
                        else if (this.state.player === 1) {
                            bgcolor = this.state.cw;
                        }

                        r.setState ({className: player, bgcolor: bgcolor});
                    }
                });
            });

        }
    }

    handleMouseout (e, r, m, n) {
        if (r?.setState && this?.state?.buttons && (this?.state?.win === null)) {
            this.state.buttons.forEach((nv, ni) => {
                return nv.forEach((mv, mi) => {
                    if ((n === ni) && (m === mi) && (mv === null)) {
                        r.setState ({className: null, bgcolor: null});

                    }
                });
            });

        }
    }

    generateClassName(p, m, n) {
        let className;
        if ((m === 0) && (n === 0)) {
            className = '00';
        }
        else if ((m === (this.state.m - 1)) && (n === 0)) {
            className = '10';
        }
        else if ((m < (this.state.m - 1)) && (n === 0)) {
            className = 't0';
        }
        else if ((m === 0) && (n === (this.state.m - 1))) {
            className = '01';
        }
        else if ((m === 0) && (n < (this.state.m - 1))) {
            className = '0l';
        }
        else if (((m === Math.floor(this.state.m / 2)) && (n === Math.floor(this.state.n / 2)))
         || (((this.state.m - m) === 4) && (n === 3))
         || ((m === 3) && ((this.state.n - n) === 4))
         || (((this.state.m - m) === 4) && ((this.state.n - n) === 4))
         || ((m === 3) && (n === 3))) {
            className = 'd';
        }
        else if ((m === (this.state.m - 1)) && (n === (this.state.m - 1))) {
            className = '11';
        }
        else if ((m === (this.state.m - 1)) && (n < (this.state.m - 1))) {
            className = '1r';
        }
        else if ((m < (this.state.m - 1)) && (n === (this.state.m - 1))) {
            className = 'b1';
        }
        if (className) {
            className = p + className;
        }
        else {
            className = p;
        }

        return className;
    }

    generateBoard (mbtStyle) {
        return this.state.buttons.map((p, n) => <div className='MnkRow' key={"n" + n}>{p.map((b, m) => <MnkButton onClick={this.handleClick} onMouseOver={this.handleMouseover} onMouseOut={this.handleMouseout} style={mbtStyle} className={this.generateClassName('MnkButton', m, n)} v={b} m={m} n={n} key={"m" + m + "_n" + n} />)}</div>);
    }

    undoBoard () {
        if (this.state.history && (this.state.win == null)) {
            let hl = this.state.history.length;
            if (hl) {
                let history = this.state.history.map(e => e);
                let hr = history.pop();
                let m = hr.props.m, n = hr.props.n;
                let step = this.state.step - 1;
                hr.setState ({className: null, value: null, bgcolor: null});
                let player = this.state.player;
                if (player === 0) {
                    player = 1;
                }
                else if (player === 1) {
                    player = 0;
                }
                const copied = this.state.buttons.map((nv, ni) => {
                    return nv.map((mv, mi) => {
                        if ((n === ni) && (m === mi) && (mv !== null)) {
                            return null;
                        }
                        else {
                            return mv;
    
                        }
                    });
                });
    
                this.setState({buttons: copied, history: history, player: player, step: step});

            }
        }

    }

    render () {
        let scope = this;
        if (this.state.m && this.state.n) {
            let minhw = 90.0, mbthw, mbphw, mbfhw, mophw = 9.0;
            if (this.state.height > this.state.width) {
                mbfhw = minhw / this.state.m;
                mbthw = mbfhw + 'vw';
                mbphw = mbfhw * 3 + 'vw';
                mophw = mophw + 'vw';
                mbfhw = (mbfhw / 4) + 'vw';
            }
            else {
                mbfhw = (minhw / this.state.n);
                mbthw = mbfhw + 'vh';
                mbphw = mbfhw * 3 + 'vh';
                mophw = mophw + 'vh';
                mbfhw = (mbfhw / 3) + 'vh';
            }
            let mbtStyle = {height: mbthw, width: mbthw, fontSize: mbfhw};
            let mopStyle = {height: mophw, flexDirection: 'row'};
            let mnk = [{name: '9x9', index: 0, value: {m: 9, n: 9}}, {name: '13x13', index: 1, value: {m: 13, n: 13}}, {name: '15x15', index: 2, value: {m: 15, n: 15}}, {name: '19x19', index: 3, value: {m: 19, n: 19}}];
            let k = [5,6];
            let mni = mnk.findIndex(e => ((e.value.m === scope.state.m) && (e.value.n === scope.state.n)));

            return (
            <div className='Mnk' key='mnkBoard'>
                <Box className='MnkPanel' sx={mopStyle}>
                    <FormControl>
                        <InputLabel id="mn-select-label"><BorderAllIcon/></InputLabel>
                        <Select labelId="mn-select-label" id="mn-select" label="mn" value={mnk[mni !== - 1 ? mni : 2].value} disabled={scope?.state?.history?.length !== 0} onChange={scope.handleChange}>
                            {mnk.map(e => <MenuItem key={'mn_' + e.index} value={e.value}>{e.name}</MenuItem>)}
                        </Select>
                    </FormControl>
                
                
                    <IconButton aria-label='restart' onClick={() => this.initBoard(this, true)} disabled={scope?.state?.history?.length === 0} ><RestartAltIcon/></IconButton>
                    <Avatar sx={{height: mbthw, width: mbthw, bgcolor: this.state.cb}} variant={this?.state?.player === 0 ? 'circular': 'rounded'} children={'1'.split(' ')[0][0]}>
                        <img src={logo} className={(this?.state?.win === null) ? 'App-logo' : (this?.state?.win === 0 ? 'App-logo-win' : 'App-logo')} alt="logo" />
                    </Avatar>
                    <Box sx={{width: mbphw, margin: '0 ' + mbthw}}>
                        <LinearProgress sx={{width: mbphw, bgcolor: '#C0C0C090', '& .MuiLinearProgress-bar1Determinate': {bgcolor: this?.state?.player === 0 ? this.state.cb : this.state.cw}}} variant='determinate' value={this.state.countdown / this.state.timer * 100}/>
                    </Box>
                    
                    <Avatar sx={{height: mbthw, width: mbthw, bgcolor: this.state.cw}} variant={this?.state?.player === 1 ? 'circular': 'rounded'} children={'2'.split(' ')[0][0]}>
                    <img src={logo} className={(this?.state?.win === null) ? 'App-logo' : (this?.state?.win === 1 ? 'App-logo-win' : 'App-logo')} alt="logo" />
                    </Avatar>
                    <IconButton aria-label='undo' onClick={() => this.undoBoard()} disabled={scope?.state?.history?.length === 0} ><Undo/></IconButton>
                    <FormControl>
                        <InputLabel id="k-select-label"><BorderRight/></InputLabel>
                        <Select labelId="k-select-label" id="k-select" label="k" value={this.state.k} disabled={scope?.state?.history?.length !== 0} onChange={scope.handleChange}>
                            {k.map(e => <MenuItem key={'k_' + e} value={e}>{e}</MenuItem>)}
                        </Select>
                    </FormControl>
                    
                </Box>

                {this.generateBoard (mbtStyle)}

            </div>);

        }
        else {
            return (<div className='Mnk' key='mnkBoard_default'>
                <button/>
            </div>);
        }
    }

    componentDidMount() {
        this.initBoard3D ();//this.drawBoard3D(this.state.m, this.state.n, this.state.k, this);

    }

    initBoard3D () {
        let width = window.innerWidth, height = window.innerHeight, scene, camera, renderer, gridHelper;
        let lwh = width > height ? height * 0.5 : width * 0.5;
        scene = new THREE.Scene();
        camera = new THREE.OrthographicCamera( width / - 2, width / 2, height / 2, height / - 2, 1, 1000 );
        camera.position.z = 1;
        gridHelper = new THREE.GridHelper( lwh * 1.8 * (this.state.m - 1) / this.state.m, this.state.m - 1, 0x888888, 0x888888 );
        gridHelper.rotation.x = Math.PI * 0.5;
        gridHelper.position.z = 0 - lwh * 0.2;
        gridHelper.position.y = 0 - lwh * 0.08;
        scene.add( gridHelper );
        
        renderer = new THREE.WebGLRenderer( { antialias: true } );
        renderer.setSize( window.innerWidth, window.innerHeight );
        renderer.setClearColor(0x1E1E1E);
        renderer.setAnimationLoop( animation );
        document.getElementById('root').appendChild( renderer.domElement );
        renderer.domElement.style.position = 'fixed';
        renderer.domElement.style.top = 0;
        renderer.domElement.style.zIndex = -1;
        this.setState({camera: camera, renderer: renderer, scene: scene});
        window.onresize = this.updateBoard3D;
        // animation
        function animation( time ) {
            renderer.render( scene, camera );
        }

    }

    updateBoard3D () {
        let width = window.innerWidth, height = window.innerHeight, scene, camera, renderer, gridHelper;
        camera = this.state.camera;
        renderer = this.state.renderer;
        scene = this.state.scene;
        width = window.innerWidth;
        height = window.innerHeight;
        let lwh = width > height ? height * 0.5 : width * 0.5;
        gridHelper = new THREE.GridHelper( lwh * 1.8 * (this.state.m - 1) / this.state.m, this.state.m - 1, 0x888888, 0x888888 );
        gridHelper.rotation.x = Math.PI * 0.5;
        gridHelper.position.z = 0 - lwh * 0.2;
        gridHelper.position.y = 0 - lwh * 0.08;
        scene.clear();
        scene.add( gridHelper );
        camera.left = width / - 2;
        camera.right = width / 2;
        camera.top = height / 2;
        camera.bottom = height / - 2;
        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();
        renderer.setSize( window.innerWidth, window.innerHeight );

    }

}

export default MnkBoard;
